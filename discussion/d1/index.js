//   alert("Hello");

// console.log("Hello World");
// console.
// log
// (
//   "Hello Again"
// )  
    
// Variables
// - used to store date
// - any info that is used by an app is stored in what we called "memory"
// - initialized with the use of let/const keyword
// not defined - non existing
// undefined - existing but no value

// let myVariable; 
// **declaring a variable (and value like "Hello")

// let myVariable = "Hello"
// **initializing (with value)

// console.log(myVariable);
// **calling a variable

// let myVariable = "Hello"; 
// console.log(myVariable);
// **Rendered hello (declared and initialized or stored something inside memory)

// let - changeable
// const - unchangeable

// GUIDES IN WRITING VARIABLES
// 1. right keyword to successfully initialized (let/const)
// 2. camelCasing for multiple words or use underscore (not hyphen)
// 3. var names should be indicative / descriptive of the value being stored to avoid confusion

// SAMPLE

// let firstName = "Michael";
// let pokemon = 25000;

// let myVariable = "Hello"; 
// console.log(myVariable);


// DECLARING and INITIALIZING VARIABLES

// let productName = "Desktop Computer";
// console.log(productName);

// Create a product
// let productPrice = "18999";
// console.log (productPrice);

// *Constant and should not be changed (ex. interest for loan, mortgage)
// const interest = 3.539;
// console.log(interest);

// Reassigning of Variables

// let productName = "Desktop Computer";
// console.log(productName);

// productName = "Laptop";
// console.log(productName);

// MINIACTIVITY : Reassigning a variable
// let friend = "Alvin";
// console.log("friend");

// friend = "Mike";
// console.log(friend);

// const friend = "Alvin";
// console.log("friend");

// friend = "Mike";
// console.log(friend);

// SCOPE OF VARIABLE

// let outerVariable

// let country = "England";
// console.log(country);

// let mailAddress = "Metro Manila\n\nPhilippines"
// console.log(mailAddress);


console.log("John's employees");

// 
let headcount = 26;
console.log(headcount);

// 
let grade = 98.7;
console.log(grade);

// 
let planetDistance = 2e10;
console.log(planetDistance);

let grades = [98.7, 92.1, 90.2, 94.6];
console.log(grades);

let person = ["John", "Smith", 32, true];
console.log(person);

